package com.xy.yupao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xy.yupao.model.domain.UserTeam;

/**
 * @Entity UserTeam
 */
public interface UserTeamMapper extends BaseMapper<UserTeam> {

}





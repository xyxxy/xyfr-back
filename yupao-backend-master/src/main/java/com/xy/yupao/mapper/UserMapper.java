package com.xy.yupao.mapper;

import com.xy.yupao.model.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity User
 */
public interface UserMapper extends BaseMapper<User> {

}





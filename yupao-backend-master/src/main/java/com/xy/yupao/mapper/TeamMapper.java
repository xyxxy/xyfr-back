package com.xy.yupao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xy.yupao.model.domain.Team;

/**
 * @Entity Team
 */
public interface TeamMapper extends BaseMapper<Team> {

}





package com.xy.yupao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xy.yupao.model.domain.UserTeam;

/**
 *
 */
public interface UserTeamService extends IService<UserTeam> {

}
